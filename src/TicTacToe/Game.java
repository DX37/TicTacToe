package TicTacToe;

import java.awt.*;
import java.util.Objects;
import java.util.Random;
import javax.swing.*;

public class Game extends javax.swing.JFrame {
    private boolean isEnable1 = false;
    private boolean isEnable2 = false;
    private boolean isEnable3 = false;
    private boolean isEnable4 = false;
    private boolean isEnable5 = false;
    private boolean isEnable6 = false;
    private boolean isEnable7 = false;
    private boolean isEnable8 = false;
    private boolean isEnable9 = false;
    private boolean GameState = true;
    private boolean isButtonTextChange = false;

    private String s1, s2, s3, s4, s5, s6, s7, s8, s9;
    private int DestinyState;
    private int chooseOpponent;
    private int chooseAI;
    private int isComp;
    private int HowMuchWays;

    private void AISet(String a) {

        int test = new Random().nextInt(9) + 1;

        if ((test == 1) && (s1.equals(""))) {
            jButton1.setText(a);
            isButtonTextChange = true;
            isEnable1 = true;
        } else if ((test == 2) && (s2.equals(""))) {
            jButton2.setText(a);
            isButtonTextChange = true;
            isEnable2 = true;
        } else if ((test == 3) && (s3.equals(""))) {
            jButton3.setText(a);
            isButtonTextChange = true;
            isEnable3 = true;
        } else if ((test == 4) && (s4.equals(""))) {
            jButton4.setText(a);
            isButtonTextChange = true;
            isEnable4 = true;
        } else if ((test == 5) && (s5.equals(""))) {
            jButton5.setText(a);
            isButtonTextChange = true;
            isEnable5 = true;
        } else if ((test == 6) && (s6.equals(""))) {
            jButton6.setText(a);
            isButtonTextChange = true;
            isEnable6 = true;
        } else if ((test == 7) && (s7.equals(""))) {
            jButton7.setText(a);
            isButtonTextChange = true;
            isEnable7 = true;
        } else if ((test == 8) && (s8.equals(""))) {
            jButton8.setText(a);
            isButtonTextChange = true;
            isEnable8 = true;
        } else if ((test == 9) && (s9.equals(""))) {
            jButton9.setText(a);
            isButtonTextChange = true;
            isEnable9 = true;
        }
    }

    private void AIStupid() {
        GetText();

        if (isComp == 1) {
            AISet("O");
            if (GameState)
                Check("AI");
            DestinyState = 0;
        } else if (isComp == 0) {
            AISet("X");
            if (GameState)
                Check("AI");
            DestinyState = 1;
        }
    }

    private void AICleverSet(String AI, String human, int ds) {
        int Way1 = 0, Way2 = 0, Way3 = 0, Way4 = 0, Way5 = 0, Way6 = 0, Way7 = 0, Way8 = 0;

        //Way1
        if (s1.equals(AI))
            Way1 = Way1 + 1;
        else if (s1.equals(human))
            Way1 = Way1 - 1;

        if (s2.equals(AI))
            Way1 = Way1 + 1;
        else if (s2.equals(human))
            Way1 = Way1 - 1;

        if (s3.equals(AI))
            Way1 = Way1 + 1;
        else if (s3.equals(human))
            Way1 = Way1 - 1;

        //Way2
        if (s4.equals(AI))
            Way2 = Way2 + 1;
        else if (s4.equals(human))
            Way2 = Way2 - 1;

        if (s5.equals(AI))
            Way2 = Way2 + 1;
        else if (s5.equals(human))
            Way2 = Way2 - 1;

        if (s6.equals(AI))
            Way2 = Way2 + 1;
        else if (s6.equals(human))
            Way2 = Way2 - 1;

        //Way3
        if (s7.equals(AI))
            Way3 = Way3 + 1;
        else if (s7.equals(human))
            Way3 = Way3 - 1;

        if (s8.equals(AI))
            Way3 = Way3 + 1;
        else if (s8.equals(human))
            Way3 = Way3 - 1;

        if (s9.equals(AI))
            Way3 = Way3 + 1;
        else if (s9.equals(human))
            Way3 = Way3 - 1;

        //Way4
        if (s1.equals(AI))
            Way4 = Way4 + 1;
        else if (s1.equals(human))
            Way4 = Way4 - 1;

        if (s4.equals(AI))
            Way4 = Way4 + 1;
        else if (s4.equals(human))
            Way4 = Way4 - 1;

        if (s7.equals(AI))
            Way4 = Way4 + 1;
        else if (s7.equals(human))
            Way4 = Way4 - 1;

        //Way5
        if (s2.equals(AI))
            Way5 = Way5 + 1;
        else if (s2.equals(human))
            Way5 = Way5 - 1;

        if (s5.equals(AI))
            Way5 = Way5 + 1;
        else if (s5.equals(human))
            Way5 = Way5 - 1;

        if (s8.equals(AI))
            Way5 = Way5 + 1;
        else if (s8.equals(human))
            Way5 = Way5 - 1;

        //Way6
        if (s3.equals(AI))
            Way6 = Way6 + 1;
        else if (s3.equals(human))
            Way6 = Way6 - 1;

        if (s6.equals(AI))
            Way6 = Way6 + 1;
        else if (s6.equals(human))
            Way6 = Way6 - 1;

        if (s9.equals(AI))
            Way6 = Way6 + 1;
        else if (s9.equals(human))
            Way6 = Way6 - 1;

        //Way7
        if (s1.equals(AI))
            Way7 = Way7 + 1;
        else if (s1.equals(human))
            Way7 = Way7 - 1;

        if (s5.equals(AI))
            Way7 = Way7 + 1;
        else if (s5.equals(human))
            Way7 = Way7 - 1;

        if (s9.equals(AI))
            Way7 = Way7 + 1;
        else if (s9.equals(human))
            Way7 = Way7 - 1;

        //Way8
        if (s3.equals(AI))
            Way8 = Way8 + 1;
        else if (s3.equals(human))
            Way8 = Way8 - 1;

        if (s5.equals(AI))
            Way8 = Way8 + 1;
        else if (s5.equals(human))
            Way8 = Way8 - 1;

        if (s7.equals(AI))
            Way8 = Way8 + 1;
        else if (s7.equals(human))
            Way8 = Way8 - 1;
        
        if (Way1 == 2 || Way2 == 2 || Way3 == 2 || Way4 == 2 || Way5 == 2 || Way6 == 2 || Way7 == 2 || Way8 == 2) {
            if (Way1 == 2) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s1.equals("")) {
                        jButton1.setText(AI);
                        isEnable1=true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s2.equals("")) {
                        jButton2.setText(AI);
                        isEnable2=true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s3.equals("")) {
                        jButton3.setText(AI);
                        isEnable3=true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way2 == 2) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s4.equals("")) {
                        jButton4.setText(AI);
                        isEnable4=true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                        jButton5.setText(AI);
                        isEnable5=true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s6.equals("")) {
                        jButton6.setText(AI);
                        isEnable6=true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way3 == 2) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s7.equals("")) {
                        jButton7.setText(AI);
                        isEnable7=true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s8.equals("")) {
                        jButton8.setText(AI);
                        isEnable8=true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s9.equals("")) {
                        jButton9.setText(AI);
                        isEnable9=true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way4 == 2) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s1.equals("")) {
                        jButton1.setText(AI);
                        isEnable1=true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s4.equals("")) {
                        jButton4.setText(AI);
                        isEnable4=true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s7.equals("")) {
                        jButton7.setText(AI);
                        isEnable7=true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way5 == 2) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s2.equals("")) {
                        jButton2.setText(AI);
                        isEnable2 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                        jButton5.setText(AI);
                        isEnable5 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s8.equals("")) {
                        jButton8.setText(AI);
                        isEnable8 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way6 == 2) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s3.equals("")) {
                        jButton3.setText(AI);
                        isEnable3 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s6.equals("")) {
                        jButton6.setText(AI);
                        isEnable6 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s9.equals("")) {
                        jButton9.setText(AI);
                        isEnable9 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way7 == 2) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s1.equals("")) {
                        jButton1.setText(AI);
                        isEnable1 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                        jButton5.setText(AI);
                        isEnable5 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s9.equals("")) {
                        jButton9.setText(AI);
                        isEnable9 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way8 == 2) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s3.equals("")) {
                        jButton3.setText(AI);
                        isEnable3 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                        jButton5.setText(AI);
                        isEnable5 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s7.equals("")) {
                        jButton7.setText(AI);
                        isEnable7 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
        }

        else if (Way1 == -2 || Way2 == -2 || Way3 == -2 || Way4 == -2 || Way5 == -2 || Way6 == -2 || Way7 == -2 || Way8 == -2) {
            if (Way1 == -2) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s1.equals("")) {
                        jButton1.setText(AI);
                        isEnable1 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s2.equals("")) {
                        jButton2.setText(AI);
                        isEnable2 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s3.equals("")) {
                        jButton3.setText(AI);
                        isEnable3 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way2 == -2) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s4.equals("")) {
                        jButton4.setText(AI);
                        isEnable4 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                        jButton5.setText(AI);
                        isEnable5 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s6.equals("")) {
                        jButton6.setText(AI);
                        isEnable6 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way3 == -2) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s7.equals("")) {
                        jButton7.setText(AI);
                        isEnable7 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s8.equals("")) {
                        jButton8.setText(AI);
                        isEnable8 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s9.equals("")) {
                        jButton9.setText(AI);
                        isEnable9 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way4 == -2) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s1.equals("")) {
                        jButton1.setText(AI);
                        isEnable1 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s4.equals("")) {
                        jButton4.setText(AI);
                        isEnable4 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s7.equals("")) {
                        jButton7.setText(AI);
                        isEnable7 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way5 == -2) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s2.equals("")) {
                        jButton2.setText(AI);
                        isEnable2 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                        jButton5.setText(AI);
                        isEnable5 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s8.equals("")) {
                        jButton8.setText(AI);
                        isEnable8 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way6 == -2) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s3.equals("")) {
                        jButton3.setText(AI);
                        isEnable3 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s6.equals("")) {
                        jButton6.setText(AI);
                        isEnable6 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s9.equals("")) {
                        jButton9.setText(AI);
                        isEnable9 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way7 == -2) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s1.equals("")) {
                        jButton1.setText(AI);
                        isEnable1 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                        jButton5.setText(AI);
                        isEnable5 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s9.equals("")) {
                        jButton9.setText(AI);
                        isEnable9 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way8 == -2) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s3.equals("")) {
                        jButton3.setText(AI);
                        isEnable3 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                        jButton5.setText(AI);
                        isEnable5 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s7.equals("")) {
                        jButton7.setText(AI);
                        isEnable7 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
        }

        else if (Way1 == 1 || Way2 == 1 || Way3 == 1 || Way4 == 1 || Way5 == 1 || Way6 == 1 || Way7 == 1 || Way8 == 1) {
            if (Way1 == 1 && (s1.equals("") || s2.equals("") || s3.equals(""))) {
                HowMuchWays++;
            }
            if (Way2 == 1 && (s4.equals("") || s5.equals("") || s6.equals(""))) {
                HowMuchWays++;
            }
            if (Way3 == 1 && (s7.equals("") || s8.equals("") || s9.equals(""))) {
                HowMuchWays++;
            }
            if (Way4 == 1 && (s1.equals("") || s4.equals("") || s7.equals(""))) {
                HowMuchWays++;
            }
            if (Way5 == 1 && (s2.equals("") || s5.equals("") || s8.equals(""))) {
                HowMuchWays++;
            }
            if (Way6 == 1 && (s3.equals("") || s6.equals("") || s9.equals(""))) {
                HowMuchWays++;
            }
            if (Way7 == 1 && (s1.equals("") || s5.equals("") || s9.equals(""))) {
                HowMuchWays++;
            }
            if (Way8 == 1 && (s3.equals("") || s5.equals("") || s7.equals(""))) {
                HowMuchWays++;
            }

            if (HowMuchWays >= 1) {
                if (Way1 == 1) {
                    while (!isButtonTextChange) {
                        int xpoint = new Random().nextInt(100);

                        if (xpoint >= 0 && xpoint <= 33 && s1.equals("")) {
                            jButton1.setText(AI);
                            isEnable1 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 34 && xpoint <= 66 && s2.equals("")) {
                            jButton2.setText(AI);
                            isEnable2 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 67 && xpoint <= 99 && s3.equals("")) {
                            jButton3.setText(AI);
                            isEnable3 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        }
                    }
                }
                if (Way2 == 1) {
                    while (!isButtonTextChange) {
                        int xpoint = new Random().nextInt(100);

                        if (xpoint >= 0 && xpoint <= 33 && s4.equals("")) {
                            jButton4.setText(AI);
                            isEnable4 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                            jButton5.setText(AI);
                            isEnable5 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 67 && xpoint <= 99 && s6.equals("")) {
                            jButton6.setText(AI);
                            isEnable6 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        }
                    }
                }
                if (Way3 == 1) {
                    while (!isButtonTextChange) {
                        int xpoint = new Random().nextInt(100);

                        if (xpoint >= 0 && xpoint <= 33 && s7.equals("")) {
                            jButton7.setText(AI);
                            isEnable7 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 34 && xpoint <= 66 && s8.equals("")) {
                            jButton8.setText(AI);
                            isEnable8 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 67 && xpoint <= 99 && s9.equals("")) {
                            jButton9.setText(AI);
                            isEnable9 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        }
                    }
                }
                if (Way4 == 1) {
                    while (!isButtonTextChange) {
                        int xpoint = new Random().nextInt(100);

                        if (xpoint >= 0 && xpoint <= 33 && s1.equals("")) {
                            jButton1.setText(AI);
                            isEnable1 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 34 && xpoint <= 66 && s4.equals("")) {
                            jButton4.setText(AI);
                            isEnable4 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 67 && xpoint <= 99 && s7.equals("")) {
                            jButton7.setText(AI);
                            isEnable7 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        }
                    }
                }
                if (Way5 == 1) {
                    while (!isButtonTextChange) {
                        int xpoint = new Random().nextInt(100);

                        if (xpoint >= 0 && xpoint <= 33 && s2.equals("")) {
                            jButton2.setText(AI);
                            isEnable2 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                            jButton5.setText(AI);
                            isEnable5 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 67 && xpoint <= 99 && s8.equals("")) {
                            jButton8.setText(AI);
                            isEnable8 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        }
                    }
                }
                if (Way6 == 1) {
                    while (!isButtonTextChange) {
                        int xpoint = new Random().nextInt(100);

                        if (xpoint >= 0 && xpoint <= 33 && s3.equals("")) {
                            jButton3.setText(AI);
                            isEnable3 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 34 && xpoint <= 66 && s6.equals("")) {
                            jButton6.setText(AI);
                            isEnable6 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 67 && xpoint <= 99 && s9.equals("")) {
                            jButton9.setText(AI);
                            isEnable9 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        }
                    }
                }
                if (Way7 == 1) {
                    while (!isButtonTextChange) {
                        int xpoint = new Random().nextInt(100);

                        if (xpoint >= 0 && xpoint <= 33 && s1.equals("")) {
                            jButton1.setText(AI);
                            isEnable1 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                            jButton5.setText(AI);
                            isEnable5 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 67 && xpoint <= 99 && s9.equals("")) {
                            jButton9.setText(AI);
                            isEnable9 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        }
                    }
                }
                if (Way8 == 1) {
                    while (!isButtonTextChange) {
                        int xpoint = new Random().nextInt(100);

                        if (xpoint >= 0 && xpoint <= 33 && s3.equals("")) {
                            jButton3.setText(AI);
                            isEnable3 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                            jButton5.setText(AI);
                            isEnable5 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 67 && xpoint <= 99 && s7.equals("")) {
                            jButton7.setText(AI);
                            isEnable7 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        }
                    }
                }
            }
        }
        else if (Way1 == -1 || Way2 == -1 || Way3 == -1 || Way4 == -1 || Way5 == -1 || Way6 == -1 || Way7 == -1 || Way8 == -1) {
            if (Way1 == -1 && (s1.equals("") || s2.equals("") || s3.equals(""))) {
                HowMuchWays++;
            }
            if (Way2 == -1 && (s4.equals("") || s5.equals("") || s6.equals(""))) {
                HowMuchWays++;
            }
            if (Way3 == -1 && (s7.equals("") || s8.equals("") || s9.equals(""))) {
                HowMuchWays++;
            }
            if (Way4 == -1 && (s1.equals("") || s4.equals("") || s7.equals(""))) {
                HowMuchWays++;
            }
            if (Way5 == -1 && (s2.equals("") || s5.equals("") || s8.equals(""))) {
                HowMuchWays++;
            }
            if (Way6 == -1 && (s3.equals("") || s6.equals("") || s9.equals(""))) {
                HowMuchWays++;
            }
            if (Way7 == -1 && (s1.equals("") || s5.equals("") || s9.equals(""))) {
                HowMuchWays++;
            }
            if (Way8 == -1 && (s3.equals("") || s5.equals("") || s7.equals(""))) {
                HowMuchWays++;
            }

            if (HowMuchWays >= 1) {
                if (Way1 == -1) {
                    while (!isButtonTextChange) {
                        int xpoint = new Random().nextInt(100);

                        if (xpoint >= 0 && xpoint <= 33 && s1.equals("")) {
                            jButton1.setText(AI);
                            isEnable1 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 34 && xpoint <= 66 && s2.equals("")) {
                            jButton2.setText(AI);
                            isEnable2 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 67 && xpoint <= 99 && s3.equals("")) {
                            jButton3.setText(AI);
                            isEnable3 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        }
                    }
                }
                if (Way2 == -1) {
                    while (!isButtonTextChange) {
                        int xpoint = new Random().nextInt(100);

                        if (xpoint >= 0 && xpoint <= 33 && s4.equals("")) {
                            jButton4.setText(AI);
                            isEnable4 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                            jButton5.setText(AI);
                            isEnable5 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 67 && xpoint <= 99 && s6.equals("")) {
                            jButton6.setText(AI);
                            isEnable6 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        }
                    }
                }
                if (Way3 == -1) {
                    while (!isButtonTextChange) {
                        int xpoint = new Random().nextInt(100);

                        if (xpoint >= 0 && xpoint <= 33 && s7.equals("")) {
                            jButton7.setText(AI);
                            isEnable7 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 34 && xpoint <= 66 && s8.equals("")) {
                            jButton8.setText(AI);
                            isEnable8 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 67 && xpoint <= 99 && s9.equals("")) {
                            jButton9.setText(AI);
                            isEnable9 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        }
                    }
                }
                if (Way4 == -1) {
                    while (!isButtonTextChange) {
                        int xpoint = new Random().nextInt(100);

                        if (xpoint >= 0 && xpoint <= 33 && s1.equals("")) {
                            jButton1.setText(AI);
                            isEnable1 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 34 && xpoint <= 66 && s4.equals("")) {
                            jButton4.setText(AI);
                            isEnable4 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 67 && xpoint <= 99 && s7.equals("")) {
                            jButton7.setText(AI);
                            isEnable7 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        }
                    }
                }
                if (Way5 == -1) {
                    while (!isButtonTextChange) {
                        int xpoint = new Random().nextInt(100);

                        if (xpoint >= 0 && xpoint <= 33 && s2.equals("")) {
                            jButton2.setText(AI);
                            isEnable2 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                            jButton5.setText(AI);
                            isEnable5 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 67 && xpoint <= 99 && s8.equals("")) {
                            jButton8.setText(AI);
                            isEnable8 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        }
                    }
                }
                if (Way6 == -1) {
                    while (!isButtonTextChange) {
                        int xpoint = new Random().nextInt(100);

                        if (xpoint >= 0 && xpoint <= 33 && s3.equals("")) {
                            jButton3.setText(AI);
                            isEnable3 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 34 && xpoint <= 66 && s6.equals("")) {
                            jButton6.setText(AI);
                            isEnable6 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 67 && xpoint <= 99 && s9.equals("")) {
                            jButton9.setText(AI);
                            isEnable9 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        }
                    }
                }
                if (Way7 == -1) {
                    while (!isButtonTextChange) {
                        int xpoint = new Random().nextInt(100);

                        if (xpoint >= 0 && xpoint <= 33 && s1.equals("")) {
                            jButton1.setText(AI);
                            isEnable1 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                            jButton5.setText(AI);
                            isEnable5 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 67 && xpoint <= 99 && s9.equals("")) {
                            jButton9.setText(AI);
                            isEnable9 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        }
                    }
                }
                if (Way8 == -1) {
                    while (!isButtonTextChange) {
                        int xpoint = new Random().nextInt(100);

                        if (xpoint >= 0 && xpoint <= 33 && s3.equals("")) {
                            jButton3.setText(AI);
                            isEnable3 = true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                            jButton5.setText(AI);
                            isEnable5= true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        } else if (xpoint >= 67 && xpoint <= 99 && s7.equals("")) {
                            jButton7.setText(AI);
                            isEnable7= true;
                            if (GameState)
                                Check("AI");
                            DestinyState = ds;
                            isButtonTextChange = true;
                        }
                    }
                }
            }
        } else if (Way1 == 0 || Way2 == 0 || Way3 == 0 || Way4 == 0 || Way5 == 0 || Way6 == 0 || Way7 == 0 || Way8 == 0) {
            if (Way1 == 0) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s1.equals("")) {
                        jButton1.setText(AI);
                        isEnable1 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s2.equals("")) {
                        jButton2.setText(AI);
                        isEnable2 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s3.equals("")) {
                        jButton3.setText(AI);
                        isEnable3 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way2 == 0) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s4.equals("")) {
                        jButton4.setText(AI);
                        isEnable4 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                        jButton5.setText(AI);
                        isEnable5 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s6.equals("")) {
                        jButton6.setText(AI);
                        isEnable6 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way3 == 0) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s7.equals("")) {
                        jButton7.setText(AI);
                        isEnable7 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s8.equals("")) {
                        jButton8.setText(AI);
                        isEnable8 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s9.equals("")) {
                        jButton9.setText(AI);
                        isEnable9 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way4 == 0) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s1.equals("")) {
                        jButton1.setText(AI);
                        isEnable1 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s4.equals("")) {
                        jButton4.setText(AI);
                        isEnable4 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s7.equals("")) {
                        jButton7.setText(AI);
                        isEnable7 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way5 == 0) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s2.equals("")) {
                        jButton2.setText(AI);
                        isEnable2 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                        jButton5.setText(AI);
                        isEnable5 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s8.equals("")) {
                        jButton8.setText(AI);
                        isEnable8 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way6 == 0) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s3.equals("")) {
                        jButton3.setText(AI);
                        isEnable3 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s6.equals("")) {
                        jButton6.setText(AI);
                        isEnable6 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s9.equals("")) {
                        jButton9.setText(AI);
                        isEnable9 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way7 == 0) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s1.equals("")) {
                        jButton1.setText(AI);
                        isEnable1 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                        jButton5.setText(AI);
                        isEnable5 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s9.equals("")) {
                        jButton9.setText(AI);
                        isEnable9 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
            if (Way8 == 0) {
                while (!isButtonTextChange) {
                    int xpoint = new Random().nextInt(100);

                    if (xpoint >= 0 && xpoint <= 33 && s3.equals("")) {
                        jButton3.setText(AI);
                        isEnable3 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 34 && xpoint <= 66 && s5.equals("")) {
                        jButton5.setText(AI);
                        isEnable5 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    } else if (xpoint >= 67 && xpoint <= 99 && s7.equals("")) {
                        jButton7.setText(AI);
                        isEnable7 = true;
                        if (GameState)
                            Check("AI");
                        DestinyState = ds;
                        isButtonTextChange = true;
                    }
                }
            }
        }
    }

    private void AIClever() {
        GetText();

        if (isComp == 1) {
            AICleverSet("O", "X", 0);
        } else if (isComp == 0) {
            AICleverSet("X", "O", 1);
        }
    }

    private void SetDisable() {
        if (!GameState) {
            jButton1.setEnabled(false);
            jButton2.setEnabled(false);
            jButton3.setEnabled(false);
            jButton4.setEnabled(false);
            jButton5.setEnabled(false);
            jButton6.setEnabled(false);
            jButton7.setEnabled(false);
            jButton8.setEnabled(false);
            jButton9.setEnabled(false);
        }
    }

    private void GetText() {
        s1 = jButton1.getText();
        s2 = jButton2.getText();
        s3 = jButton3.getText();
        s4 = jButton4.getText();
        s5 = jButton5.getText();
        s6 = jButton6.getText();
        s7 = jButton7.getText();
        s8 = jButton8.getText();
        s9 = jButton9.getText();
    }

    private void Standoff() {
        if ((!s1.equals("")) && (!s2.equals("")) && (!s3.equals("")) && (!s4.equals("")) && (!s5.equals("")) && (!s6.equals("")) && (!s7.equals("")) && (!s8.equals("")) && (!s9.equals("")) && GameState) {
                JOptionPane.showMessageDialog(this, "Ничья!", "Ничья", JOptionPane.INFORMATION_MESSAGE);
                GameState = false;
                SetDisable();
                jLabel1.setText("Ничья!");
            }
    }

    private void Check(String a) {
        GetText();

        if (((s1.equals(s2)) && (s1.equals(s3)) && (!s1.equals(""))) || ((s4.equals(s5)) && (s4.equals(s6)) && (!s4.equals(""))) || ((s7.equals(s8)) && (s7.equals(s9)) && (!s7.equals("")))
                || ((s1.equals(s4)) && (s1.equals(s7)) && (!s1.equals(""))) || ((s2.equals(s5)) && (s2.equals(s8)) && (!s2.equals(""))) || ((s3.equals(s6)) && (s3.equals(s9)) && (!s3.equals("")))
                || ((s1.equals(s5)) && (s1.equals(s9)) && (!s1.equals(""))) || ((s3.equals(s5)) && (s3.equals(s7)) && (!s3.equals(""))))
            {
                if (Objects.equals(a, "human")) {
                    if (DestinyState == 0) {
                        JOptionPane.showMessageDialog(this, "Победили Крестики!", "Победа", JOptionPane.INFORMATION_MESSAGE);
                        GameState = false;
                        isComp = 42;
                        jLabel1.setText("Победили Крестики!");
                    } else if (DestinyState == 1) {
                        JOptionPane.showMessageDialog(this, "Победили Нолики!", "Победа", JOptionPane.INFORMATION_MESSAGE);
                        GameState = false;
                        isComp = 42;
                        jLabel1.setText("Победили Нолики!");
                    }
                } else if (Objects.equals(a, "AI")) {
                    if (isComp == 0) {
                        JOptionPane.showMessageDialog(this, "Победили Крестики!", "Победа", JOptionPane.INFORMATION_MESSAGE);
                        GameState = false;
                        jLabel1.setText("Победили Крестики!");
                    } else if (isComp == 1) {
                        JOptionPane.showMessageDialog(this, "Победили Нолики!", "Победа", JOptionPane.INFORMATION_MESSAGE);
                        GameState = false;
                        jLabel1.setText("Победили Нолики!");
                    }
                }
            }

        SetDisable();
    }

    private static void frameDisplayCenter(int sizeWidth, int sizeHeight, JFrame frame) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int locationX = (screenSize.width - sizeWidth) / 2;
        int locationY = (screenSize.height - sizeHeight) / 2;
        frame.setBounds(locationX, locationY, sizeWidth, sizeHeight);
    }

    private Game() {
        initComponents();

        frameDisplayCenter(270, 420, this);

        Object[] optionsDestiny = {"Крестики", "Нолики"};
        int chooseDestiny = JOptionPane.showOptionDialog(this, "Выберите сторону:", "Крестики-Нолики",
                JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                optionsDestiny, optionsDestiny[0]);

        if (chooseDestiny == 0) {
            jLabel1.setText("Ход - Крестиков.");
            DestinyState = 0;
        }
        else if (chooseDestiny == 1) {
            jLabel1.setText("Ход - Ноликов.");
            DestinyState = 1;
        }

        Object[] optionsOpponent = {"Компьютер", "Человек"};
        chooseOpponent = JOptionPane.showOptionDialog(this, "Выберите противника:", "Крестики-Нолики",
                        JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                optionsOpponent, optionsOpponent[0]);

        if (chooseOpponent == 0) {
            jLabel2.setText("Противник: Компьютер");
            Object[] optionsAI = {"Тупой", "Умный"};
            chooseAI = JOptionPane.showOptionDialog(this, "Выберите интеллект Компьютера:", "Крестики-Нолики",
                            JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                    optionsAI, optionsAI[0]);

            if (chooseDestiny == 0)
                isComp = 1;
            else if (chooseDestiny == 1)
                isComp = 0;
        }
        else if (chooseOpponent == 1) {
            jLabel2.setText("Противник: Человек");
        }

        this.setVisible(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        JButton jButton10 = new JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Крестики-Нолики");
        setBounds(new java.awt.Rectangle(200, 200, 250, 420));
        setMinimumSize(null);
        setResizable(false);

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton1.setMaximumSize(new java.awt.Dimension(75, 75));
        jButton1.setMinimumSize(new java.awt.Dimension(75, 75));
        jButton1.setPreferredSize(new java.awt.Dimension(75, 75));
        jButton1.addActionListener(evt -> jButton1ActionPerformed());

        jButton2.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton2.setMaximumSize(new java.awt.Dimension(75, 75));
        jButton2.setMinimumSize(new java.awt.Dimension(75, 75));
        jButton2.setPreferredSize(new java.awt.Dimension(75, 75));
        jButton2.addActionListener(evt -> jButton2ActionPerformed());

        jButton3.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton3.setMaximumSize(new java.awt.Dimension(75, 75));
        jButton3.setMinimumSize(new java.awt.Dimension(75, 75));
        jButton3.setPreferredSize(new java.awt.Dimension(75, 75));
        jButton3.addActionListener(evt -> jButton3ActionPerformed());

        jButton4.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton4.setMaximumSize(new java.awt.Dimension(75, 75));
        jButton4.setMinimumSize(new java.awt.Dimension(75, 75));
        jButton4.setPreferredSize(new java.awt.Dimension(75, 75));
        jButton4.addActionListener(evt -> jButton4ActionPerformed());

        jButton5.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton5.setMaximumSize(new java.awt.Dimension(75, 75));
        jButton5.setMinimumSize(new java.awt.Dimension(75, 75));
        jButton5.setPreferredSize(new java.awt.Dimension(75, 75));
        jButton5.addActionListener(evt -> jButton5ActionPerformed());

        jButton6.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton6.setMaximumSize(new java.awt.Dimension(75, 75));
        jButton6.setMinimumSize(new java.awt.Dimension(75, 75));
        jButton6.setPreferredSize(new java.awt.Dimension(75, 75));
        jButton6.addActionListener(evt -> jButton6ActionPerformed());

        jButton7.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton7.setMaximumSize(new java.awt.Dimension(75, 75));
        jButton7.setMinimumSize(new java.awt.Dimension(75, 75));
        jButton7.setPreferredSize(new java.awt.Dimension(75, 75));
        jButton7.addActionListener(evt -> jButton7ActionPerformed());

        jButton8.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton8.setMaximumSize(new java.awt.Dimension(75, 75));
        jButton8.setMinimumSize(new java.awt.Dimension(75, 75));
        jButton8.setPreferredSize(new java.awt.Dimension(75, 75));
        jButton8.addActionListener(evt -> jButton8ActionPerformed());

        jButton9.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton9.setMaximumSize(new java.awt.Dimension(75, 75));
        jButton9.setMinimumSize(new java.awt.Dimension(75, 75));
        jButton9.setPreferredSize(new java.awt.Dimension(75, 75));
        jButton9.addActionListener(evt -> jButton9ActionPerformed());

        jLabel1.setText("Вы играете в роли");

        jLabel2.setText("Противник: ");

        jButton10.setText("Рестарт");
        jButton10.addActionListener(evt -> jButton10ActionPerformed());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton10)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addGap(39, 39, 39)
                .addComponent(jButton10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 56, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed() {//GEN-FIRST:event_jButton1ActionPerformed
        if (!isEnable1) {
            if (DestinyState == 0) {
                jButton1.setText("X");
                Check("Human");
                Standoff();
                if (GameState)
                    jLabel1.setText("Ход - Ноликов.");
                DestinyState = 1;
                if (chooseOpponent == 0 && GameState) {
                    if (chooseAI == 0)
                        while (!isButtonTextChange) {
                            AIStupid();
                        }
                    else if (chooseAI == 1)
                        while (!isButtonTextChange)
                            AIClever();
                    jLabel1.setText("Ход - Крестиков.");
                }
                isButtonTextChange = false;
                isEnable1 = true;
                Standoff();
            } else if (DestinyState == 1) {
                jButton1.setText("O");
                Check("Human");
                Standoff();
                if (GameState)
                    jLabel1.setText("Ход - Крестиков.");
                DestinyState = 0;
                if (chooseOpponent == 0 && GameState) {
                    if (chooseAI == 0)
                        while (!isButtonTextChange)
                            AIStupid();
                    else if (chooseAI == 1)
                        while (!isButtonTextChange)
                            AIClever();
                    jLabel1.setText("Ход - Ноликов.");
                }
                isButtonTextChange = false;
                isEnable1 = true;
                Standoff();
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed() {//GEN-FIRST:event_jButton3ActionPerformed
        if (!isEnable3) {
            if (DestinyState == 0) {
                jButton3.setText("X");
                Check("Human");
                Standoff();
                if (GameState)
                    jLabel1.setText("Ход - Ноликов.");
                DestinyState = 1;
                if (chooseOpponent == 0 && GameState) {
                    if (chooseAI == 0)
                        while (!isButtonTextChange)
                            AIStupid();
                    else if (chooseAI == 1)
                        while (!isButtonTextChange)
                            AIClever();
                    jLabel1.setText("Ход - Крестиков.");
                }
                isButtonTextChange = false;
                isEnable3 = true;
                Standoff();
            } else if (DestinyState == 1) {
                jButton3.setText("O");
                Check("Human");
                Standoff();
                if (GameState)
                    jLabel1.setText("Ход - Крестиков.");
                DestinyState = 0;
                if (chooseOpponent == 0 && GameState) {
                    if (chooseAI == 0)
                        while (!isButtonTextChange)
                            AIStupid();
                    else if (chooseAI == 1)
                        while (!isButtonTextChange)
                            AIClever();
                    jLabel1.setText("Ход - Ноликов.");
                }
                isButtonTextChange = false;
                isEnable3 = true;
                Standoff();
            }
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed() {//GEN-FIRST:event_jButton2ActionPerformed
        if (!isEnable2) {
            if (DestinyState == 0) {
                jButton2.setText("X");
                Check("Human");
                Standoff();
                if (GameState)
                    jLabel1.setText("Ход - Ноликов.");
                DestinyState = 1;
                if (chooseOpponent == 0 && GameState) {
                    if (chooseAI == 0)
                        while (!isButtonTextChange)
                            AIStupid();
                    else if (chooseAI == 1)
                        while (!isButtonTextChange)
                            AIClever();
                    jLabel1.setText("Ход - Крестиков.");
                }
                isButtonTextChange = false;
                isEnable2 = true;
                Standoff();
            } else if (DestinyState == 1) {
                jButton2.setText("O");
                Check("Human");
                Standoff();
                if (GameState)
                    jLabel1.setText("Ход - Крестиков.");
                DestinyState = 0;
                if (chooseOpponent == 0 && GameState) {
                    if (chooseAI == 0)
                        while (!isButtonTextChange)
                            AIStupid();
                    else if (chooseAI == 1)
                        while (!isButtonTextChange)
                            AIClever();
                    jLabel1.setText("Ход - Ноликов.");
                }
                isButtonTextChange = false;
                isEnable2 = true;
                Standoff();
            }
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed() {//GEN-FIRST:event_jButton4ActionPerformed
        if (!isEnable4) {
            if (DestinyState == 0) {
                jButton4.setText("X");
                Check("Human");
                Standoff();
                if (GameState)
                    jLabel1.setText("Ход - Ноликов.");
                DestinyState = 1;
                if (chooseOpponent == 0 && GameState) {
                    if (chooseAI == 0)
                        while (!isButtonTextChange)
                            AIStupid();
                    else if (chooseAI == 1)
                        while (!isButtonTextChange)
                            AIClever();
                    jLabel1.setText("Ход - Крестиков.");
                }
                isButtonTextChange = false;
                isEnable4 = true;
                Standoff();
            } else if (DestinyState == 1) {
                jButton4.setText("O");
                Check("Human");
                Standoff();
                if (GameState)
                    jLabel1.setText("Ход - Крестиков.");
                DestinyState = 0;
                if (chooseOpponent == 0 && GameState) {
                    if (chooseAI == 0)
                        while (!isButtonTextChange)
                            AIStupid();
                    else if (chooseAI == 1)
                        while (!isButtonTextChange)
                            AIClever();
                    jLabel1.setText("Ход - Ноликов.");
                }
                isButtonTextChange = false;
                isEnable4 = true;
                Standoff();
            }
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed() {//GEN-FIRST:event_jButton5ActionPerformed
        if (!isEnable5) {
            if (DestinyState == 0) {
                jButton5.setText("X");
                Check("Human");
                Standoff();
                if (GameState)
                    jLabel1.setText("Ход - Ноликов.");
                DestinyState = 1;
                if (chooseOpponent == 0 && GameState) {
                    if (chooseAI == 0)
                        while (!isButtonTextChange)
                            AIStupid();
                    else if (chooseAI == 1)
                        while (!isButtonTextChange)
                            AIClever();
                    jLabel1.setText("Ход - Крестиков.");
                }
                isButtonTextChange = false;
                isEnable5 = true;
                Standoff();
            } else if (DestinyState == 1) {
                jButton5.setText("O");
                Check("Human");
                Standoff();
                if (GameState)
                    jLabel1.setText("Ход - Крестиков.");
                DestinyState = 0;
                if (chooseOpponent == 0 && GameState) {
                    if (chooseAI == 0)
                        while (!isButtonTextChange)
                            AIStupid();
                    else if (chooseAI == 1)
                        while (!isButtonTextChange)
                            AIClever();
                    jLabel1.setText("Ход - Ноликов.");
                }
                isButtonTextChange = false;
                isEnable5 = true;
                Standoff();
            }
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed() {//GEN-FIRST:event_jButton6ActionPerformed
        if (!isEnable6) {
            if (DestinyState == 0) {
                jButton6.setText("X");
                Check("Human");
                Standoff();
                if (GameState)
                    jLabel1.setText("Ход - Ноликов.");
                DestinyState = 1;
                if (chooseOpponent == 0 && GameState) {
                    if (chooseAI == 0)
                        while (!isButtonTextChange)
                            AIStupid();
                    else if (chooseAI == 1)
                        while (!isButtonTextChange)
                            AIClever();
                    jLabel1.setText("Ход - Крестиков.");
                }
                isButtonTextChange = false;
                isEnable6 = true;
                Standoff();
            } else if (DestinyState == 1) {
                jButton6.setText("O");
                Check("Human");
                Standoff();
                if (GameState)
                    jLabel1.setText("Ход - Крестиков.");
                DestinyState = 0;
                if (chooseOpponent == 0 && GameState) {
                    if (chooseAI == 0)
                        while (!isButtonTextChange)
                            AIStupid();
                    else if (chooseAI == 1)
                        while (!isButtonTextChange)
                            AIClever();
                    jLabel1.setText("Ход - Ноликов.");
                }
                isButtonTextChange = false;
                isEnable6 = true;
                Standoff();
            }
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed() {//GEN-FIRST:event_jButton7ActionPerformed
        if (!isEnable7) {
            if (DestinyState == 0) {
                jButton7.setText("X");
                Check("Human");
                Standoff();
                if (GameState)
                    jLabel1.setText("Ход - Ноликов.");
                DestinyState = 1;
                if (chooseOpponent == 0 && GameState) {
                    if (chooseAI == 0)
                        while (!isButtonTextChange)
                            AIStupid();
                    else if (chooseAI == 1)
                        while (!isButtonTextChange)
                            AIClever();
                    jLabel1.setText("Ход - Крестиков.");
                }
                isButtonTextChange = false;
                isEnable7 = true;
                Standoff();
            } else if (DestinyState == 1) {
                jButton7.setText("O");
                Check("Human");
                Standoff();
                if (GameState)
                    jLabel1.setText("Ход - Крестиков.");
                DestinyState = 0;
                if (chooseOpponent == 0 && GameState) {
                    if (chooseAI == 0)
                        while (!isButtonTextChange)
                            AIStupid();
                    else if (chooseAI == 1)
                        while (!isButtonTextChange)
                            AIClever();
                    jLabel1.setText("Ход - Ноликов.");
                }
                isButtonTextChange = false;
                isEnable7 = true;
                Standoff();
            }
        }
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton8ActionPerformed() {//GEN-FIRST:event_jButton8ActionPerformed
        if (!isEnable8) {
            if (DestinyState == 0) {
                jButton8.setText("X");
                Check("Human");
                Standoff();
                if (GameState)
                    jLabel1.setText("Ход - Ноликов.");
                DestinyState = 1;
                if (chooseOpponent == 0 && GameState) {
                    if (chooseAI == 0)
                        while (!isButtonTextChange)
                            AIStupid();
                    else if (chooseAI == 1)
                        while (!isButtonTextChange)
                            AIClever();
                    jLabel1.setText("Ход - Крестиков.");
                }
                isButtonTextChange = false;
                isEnable8 = true;
                Standoff();
            } else if (DestinyState == 1) {
                jButton8.setText("O");
                Check("Human");
                Standoff();
                if (GameState)
                    jLabel1.setText("Ход - Крестиков.");
                DestinyState = 0;
                if (chooseOpponent == 0 && GameState) {
                    if (chooseAI == 0)
                        while (!isButtonTextChange)
                            AIStupid();
                    else if (chooseAI == 1)
                        while (!isButtonTextChange)
                            AIClever();
                    jLabel1.setText("Ход - Ноликов.");
                }
                isButtonTextChange = false;
                isEnable8 = true;
                Standoff();
            }
        }
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton9ActionPerformed() {//GEN-FIRST:event_jButton9ActionPerformed
        if (!isEnable9) {
            if (DestinyState == 0) {
                jButton9.setText("X");
                Check("Human");
                Standoff();
                if (GameState)
                    jLabel1.setText("Ход - Ноликов.");
                DestinyState = 1;
                if (chooseOpponent == 0 && GameState) {
                    if (chooseAI == 0)
                        while (!isButtonTextChange)
                            AIStupid();
                    else if (chooseAI == 1)
                        while (!isButtonTextChange)
                            AIClever();
                    jLabel1.setText("Ход - Крестиков.");
                }
                isButtonTextChange = false;
                isEnable9 = true;
                Standoff();
            } else if (DestinyState == 1) {
                jButton9.setText("O");
                Check("Human");
                Standoff();
                if (GameState)
                    jLabel1.setText("Ход - Крестиков.");
                DestinyState = 0;
                if (chooseOpponent == 0 && GameState) {
                    if (chooseAI == 0)
                        while (!isButtonTextChange)
                            AIStupid();
                    else if (chooseAI == 1)
                        while (!isButtonTextChange)
                            AIClever();
                    jLabel1.setText("Ход - Ноликов.");
                }
                isButtonTextChange = false;
                isEnable9 = true;
                Standoff();
            }
        }
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton10ActionPerformed() {//GEN-FIRST:event_jButton10ActionPerformed
        this.setVisible(false);
        new Game();
    }//GEN-LAST:event_jButton10ActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(Game::new);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables

}